# In-depth analysis of insert-containing antibody transcripts (Illumina) #

Copyright (C) 2022  Mathilde Foglierini Perez

email: mathilde.foglierini-perez@chuv.ch

### SUMMARY ###

We have made available here a series of scripts to analyze non-VDJ insertions in human antibody transcripts.

The scripts are primarily intended as reference for manuscript "Different classes of genomic inserts contribute to human antibody diversity" rather than a stand-alone application.

The input of the pipeline is 300 bp paired-end reads coming from a target amplicon of the antibody transcripts (heavy or light chain). Data can be found at SRA db: PRJNA638005 accession number.

The general overview of the pipeline is represented in 'SuppFig_vdj_insert_Illumina.pdf' file.

The profiling of insert-containing antibody transcripts was processed once we got the output from the previous pipeline (see https://bitbucket.org/mathildefog/vdjinsertillumina/src/master/).

These scripts were run on Linux machines.

### LICENSES ###

This code is distributed open source under the terms of the GNU Free Documention License.


### INSTALL ###

Before the pipeline can be run, the following software are required:

a) Python 2.7 https://www.python.org/download/releases/2.7/

b) Pysam https://github.com/pysam-developers/pysam

c) Java JDK 12 https://www.oracle.com/technetwork/java/javase/downloads/index.html

d) Bowtie2 v2.3.5 http://bowtie-bio.sourceforge.net/bowtie2/index.shtml

e) Burrows-Wheeler Aligner v0.7.17 http://bio-bwa.sourceforge.net/

f) samtools v1.9 http://samtools.sourceforge.net/

g) BCFtools v1.9 https://samtools.github.io/bcftools/

h) PEAR v0.9.11 https://cme.h-its.org/exelixis/web/software/pear/doc.html


### PIPELINE ###

First we download all scripts and needed files into a directory.
After running the previous pipeline, each run as the following hierarchy:

 		$ /$PATHTOWORKINGDIRECTORY/$RUN  
 		$ ├── align  
 		$ ├── analyse  
 		$ ├── raw  
 		$ └── trimmed  

	
1. Set the variables

 		$ SCRIPTSFOLDER=/PATHTOSCRIPTSFOLDER/
 		$ RUNFOLDER=/PATHTOWORKINGDIRECTORY/
		
2. Pool all the reads mapping to the insert-containing sequences to analyze (example: donor 070 with LAIR1 insert)
  
 		$ $SCRIPTSFOLDER/poolAllReads.sh LAIR1 070 $SCRIPTSFOLDER $RUNFOLDER
		
3. Make the bowtie index of the contig sequence used as reference (taken from the output from V(D)J insert pipeline)
  
   		$ # LAIR1contig.fasta correspond to run2_insert5_LAIR1_2 contig for donor 070  
   		$ bowtie2-build LAIR1contig.fasta LAIR1contig

4. Align the merged reads to the contig

   		$ alignToOneContigAllPooledReads.sh LAIR1 070
		
5. Then we analyse the mutations and correct the read to keep only the SHM (freq >= 5%). We keep the sequences from position 9 to 425, and set the orientation of the reads ADF  or ADR (ADF for donor 070).
   We set the threshold of SHM to 0.05.
  
     	$ java -jar $SCRIPTSFOLDER/CorrectReadAndKeepRealSHMs.jar  /PATHtoLAIR1_analysis/run1_run2_070/ 070.to.LAIR1 9 425 ADF 0.05

6. A file '070.to.LAIR1_uniqueRead.fasta' is produced that will be manually checked and corrected if needed.

7. After inferring the Unmutated Common Ancestor (UCA) of the clonal family, we will infer the phylogenic tree with DNAML from PHYLIP package (see methods in 'A LAIR1 insertion generates broadly reactive antibodies against malaria variant antigens', Nature, 2016).
