#!/bin/bash
ID=$1 #can be LAIR1 
DONOR=$2

set -e

#we align the assembled read to LAIR1 contig made by the pipeline in LOCAL mode
bowtie2 --local -p 12 -x "$ID"contig -U $ID.$DONOR.allReads.fastq  | samtools view -bSu - >  $DONOR.to.$ID.notSorted.bam
samtools sort $DONOR.to.$ID.notSorted.bam -o $DONOR.to.$ID.bam -@ 12
samtools index $DONOR.to.$ID.bam
rm -f $DONOR.to.$ID.notSorted.bam

#run samtools mpileup to get the frq of SNPs -A for 'output all alternate alleles' (to add? -BQ0)
if [ ! -f $DONOR.vcf ]; then
	samtools mpileup -d900000 --skip-indels -t DP,AD,SP,INFO/AD,INFO/ADF,INFO/ADR -uf "$ID"contig.fasta $DONOR.to.$ID.bam | bcftools call -m -A - >$DONOR.to.$ID.vcf
fi

#Convert bam file into pairwise alignment txt file, we remove the unmapped reads
samtools view -F4 $DONOR.to.$ID.bam | sam2pairwise > $DONOR.to.$ID.pairwiseAlign.out

