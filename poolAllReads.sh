#!/bin/bash
ID=$1 #can be LAIR1
DONOR=$2
echo $ID $DONOR
SCRIPTSFOLDER=$3
RUNFOLDER=$4

set -e

declare -a runs=('1' '2') #example of donor 070
for runIndex in ${runs[@]}
do
	RUN="run$runIndex"
	FOLDER=$RUNFOLDER/$RUN	
	declare -a don=('1' '2' '3' '4' '5')
	for donorIndex in ${don[@]}
	do
		SAMPLE="$donorIndex"_S"$donorIndex"	
		#first we get the read id list LAIR1 containing
		IDLISTFILES=$(ls $FOLDER/align/$SAMPLE/$SAMPLE.ids.*$ID.txt)
		echo $IDLISTFILES
		for IDLISTFILE in $IDLISTFILES	
		do
			#Then we created the related PE fastq files
			gunzip -c $FOLDER/trimmed/$SAMPLE"_L001_R1_001_val_1.fq.gz" > $SAMPLE"_L001_R1_001_val_1.fq"
			cat $SAMPLE"_L001_R1_001_val_1.fq" | python $SCRIPTSFOLDER/extractReadsFromFastQFile.py $IDLISTFILE 1 > $RUN.$SAMPLE.1.fastq
			rm -f $SAMPLE"_L001_R1_001_val_1.fq"
			gunzip -c $FOLDER/trimmed/$SAMPLE"_L001_R2_001_val_2.fq.gz" > $SAMPLE"_L001_R2_001_val_2.fq"
			cat  $SAMPLE"_L001_R2_001_val_2.fq" | python $SCRIPTSFOLDER/extractReadsFromFastQFile.py $IDLISTFILE 2 > $RUN.$SAMPLE.2.fastq
			rm -f $SAMPLE"_L001_R2_001_val_2.fq"

			#we merged the PE reads
			pear -f $RUN.$SAMPLE.1.fastq -r $RUN.$SAMPLE.2.fastq -o $RUN.$SAMPLE > $RUN.$SAMPLE.pear.out

			#we concatenate all assembled fast files in one
			cat $RUN.$SAMPLE.assembled.fastq >> $ID.$DONOR.allReads.fastq

			#we remove the unused files
			rm -f $RUN.$SAMPLE.*
		done
	done
done
